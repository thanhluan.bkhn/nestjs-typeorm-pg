/// <reference path="./index.ts" />

export namespace Core.Utils {
    export const removeAccents = str => {
        const accentsMap = [
            "aàảãáạăằẳẵắặâầẩẫấậ",
            "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
            "dđ",
            "DĐ",
            "eèẻẽéẹêềểễếệ",
            "EÈẺẼÉẸÊỀỂỄẾỆ",
            "iìỉĩíị",
            "IÌỈĨÍỊ",
            "oòỏõóọôồổỗốộơờởỡớợ",
            "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
            "uùủũúụưừửữứự",
            "UÙỦŨÚỤƯỪỬỮỨỰ",
            "yỳỷỹýỵ",
            "YỲỶỸÝỴ"
        ];
        for (let i = 0; i < accentsMap.length; i++) {
            let re = new RegExp("[" + accentsMap[i].substr(1) + "]", "g");
            let char = accentsMap[i][0];
            str = str.replace(re, char);
        }
        return str;
    };
    export const createInternalString = (text: string): string => {
        return removeAccents(text.trim().toLowerCase()).split(" ").filter(item => !!item).join("_");
    }
    export function camelCaseToSnakeCase(text: string): string {
        return removeAccents(text.trim()).split(/(?=[A-Z])/).join('_').toLowerCase()
    }
    export function snakeCaseToCamelCase(text: string): string {
        return removeAccents(text.trim()).replace(/-([a-z])/g, (ch) => { return ch[1].toUpperCase(); });
    }
    export const upperCaseFirstLetter = (str: string): string => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    export const roundNumber = (value) => {
        const neWvalue = Number(value) || 0;
        return parseFloat((Math.round(neWvalue * 100000) / 100000).toString());
    }

    export const makeInQuery = (list: any[], keyInItem = undefined) => {
        if (keyInItem) {
            return "'" + list.map(e => e[keyInItem] ? e[keyInItem] : "").join("','") + "'";
        }
        return "'" + list.join("','") + "'";
    }

    export const convertQtyUnitToQtyUom = ({ qty, uom, pack }) => {
        qty = +qty;
        if (qty === 0) return 0;
        if (!uom) return qty;
        if (!qty) throw '[PACK convertQtyUnitToQtyUom] QTY REQUIRED';
        if (!pack) throw '[PACK convertQtyUnitToQtyUom] PACK REQUIRED';
        if (uom === pack.packuom2 && pack.innerpack) return qty / pack.innerpack;
        if (uom === pack.packuom4 && pack.pallet) return qty / pack.pallet;
        if (uom === pack.packuom8 && pack.otherunit1) return qty / pack.otherunit1;
        if (uom === pack.packuom9 && pack.otherunit2) return qty / pack.otherunit2;
        return qty;
    }

    export const convertQtyToMasterUnit = ({ qty, uom, pack }) => {
        qty = +qty;
        if (qty === 0) return 0;
        if (!uom) return qty;
        if (!qty) throw 'QTY REQUIRED';
        if (!pack) throw 'PACK REQUIRED';
        if (uom === pack.packuom3 && pack.qty) return qty * pack.qty;
        if (uom === pack.packuom2 && pack.innerpack) return qty * pack.innerpack;
        if (uom === pack.packuom4 && pack.pallet) return qty * pack.pallet;
        return qty;
    }
}