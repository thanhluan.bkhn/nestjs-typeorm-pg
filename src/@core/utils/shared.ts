import { isNull, isNullOrUndefined } from "util";

export interface IObjectPromise<T = any> {
    [key: string]: () => Promise<T>
}
export interface IObject {
    [key: string]: string
}
const promiseWhen = (promises: Promise<any>[]) => {
    return new Promise((resolve, reject) => {
        let errors = [];
        let result = [];
        let count = 0;
        if (promises.length === 0) {
            resolve({
                errors,
                result
            });
        } else {
            promises.map((promise, idx) => {
                promise.then(res => {
                    result.push(res);
                    count += 1;
                    if (count === promises.length) {
                        resolve({
                            errors,
                            result
                        });
                    }
                }).catch(err => {
                    errors.push(err.message);
                    count += 1;
                    if (count === promises.length) {
                        resolve({
                            errors,
                            result
                        });
                    }
                });
            });
        }

    });
};
const promiseAllObject = async (promiseObj: IObjectPromise = {}) => {
    const list = [];
    for (let index = 0; index < Object.keys(promiseObj).length; index++) {
        const key = Object.keys(promiseObj)[index];
        list.push(promiseObj[key]())
    }
    const res = await Promise.all(list)
    const output = {};
    for (let index = 0; index < Object.keys(promiseObj).length; index++) {
        const key = Object.keys(promiseObj)[index];
        Object.assign(output, {
            [key]: res[index]
        })
    }
    return output;
}
const objToQueryString = (obj = {}) => {
    const keyValuePairs = [];
    for (const key in obj) {
        keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
    }
    return keyValuePairs.join('&');
}


export const SharedUtil = {
    promiseWhen,
    promiseAllObject
}