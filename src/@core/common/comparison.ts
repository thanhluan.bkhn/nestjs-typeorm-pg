import { FindManyOptions, FindOneOptions, Equal, MoreThan, MoreThanOrEqual, LessThan, LessThanOrEqual, In, Not, Like } from "typeorm";
export type TOperator = '$eq' | '$gt' | '$gte' | '$in'
    | '$lt' | '$lte' | '$ne' | '$nin'
    | '$like' | '$nlike'

export type TComparison<T> = {
    [key in TOperator]?: T
};
type TypeValue = number | string | Date | boolean
type TyepArrayValue = Array<TypeValue>
export type IWhere<Entity = any> = {
    [key in keyof Entity]?: TypeValue | TComparison<TypeValue | TyepArrayValue>
}

const formatQuery = (options: {
    key: string,
    comparison: TypeValue | TComparison<TypeValue | TyepArrayValue>
}) => {
    const { key, comparison } = options
    if (typeof comparison !== 'object') {
        return {
            [key]: comparison
        }
    }
    const comparisonKey = Object.keys(comparison)[0] as TOperator;
    const value = comparison[comparisonKey];
    switch (comparisonKey) {
        case '$eq':
            return {
                [key]: value
            }
        case '$ne':
            return {
                [key]: Not(value)
            }
        case '$gt':
            return {
                [key]: MoreThan(value)
            }
        case '$gte':
            return {
                [key]: MoreThanOrEqual(value)
            }
        case '$lt':
            return {
                [key]: LessThan(value)
            }
        case '$lte':
            return {
                [key]: LessThanOrEqual(value)
            }
        case '$in':
            return {
                [key]: In(value)
            }
        case '$nin':
            return {
                [key]: Not(In(value))
            }
        case '$like':
            return {
                [key]: Like(value)
            }
        case '$nlike':
            return {
                [key]: Not(Like(value))
            }
        default:
            return {};

    }

}
export const  mapWhere = <Entity = any>(whereClause: IWhere<Entity> = {}) => {

    let output = {};
    Object.keys(whereClause).forEach(key => {
        const comparison = whereClause[key];
        const obj = formatQuery({
            key,
            comparison
        })
        output = {
            ...output,
            ...obj
        }
    })
    return output;
}
export interface IFindOneOptions<Entity = any> extends FindOneOptions {
    where?: IWhere<Entity>;
}
export interface IFindManyOptions<Entity = any> extends IFindOneOptions<Entity> {
    pageIndex?: number
    pageSize?: number
}