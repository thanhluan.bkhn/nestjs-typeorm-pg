import { validateModuleKeys } from "@nestjs/common/utils/validate-module-keys.util";
import { MODULE_PATH } from "@nestjs/common/constants";
import { ModuleMetadata } from "@nestjs/common/interfaces";

export interface IChildModuleMetadata extends ModuleMetadata {
    prefix: string,
}

export function ChildModule(childMetadata?: IChildModuleMetadata): ClassDecorator {
    const { prefix, ...metadata } = childMetadata;
    const propsKeys = Object.keys(metadata);
    validateModuleKeys(propsKeys);
    return (target) => {
        for (const property in metadata) {
            if (metadata.hasOwnProperty(property)) {
                Reflect.defineMetadata(property, metadata[property], target);
            }
        }
        let defPrefix = "";
        if (prefix) {

            const regexConfig = /(^\/+|\/+$)/mg;
            defPrefix = prefix.replace(regexConfig, "")
            Reflect.defineMetadata(MODULE_PATH, "/" + defPrefix, target);
        }
    };
}