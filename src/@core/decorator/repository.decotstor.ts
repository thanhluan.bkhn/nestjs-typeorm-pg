
import { getCustomRepository, getRepository, ObjectType } from "typeorm";
import { TRepoClass } from "./types";
export function InjectCustomRepo(repoClass: TRepoClass): PropertyDecorator {
    const getConnectionName = repoClass.getConnectionName
        && typeof repoClass.getConnectionName == 'function' ? repoClass.getConnectionName() || 'default' : "";
    return function (target: any, propertyKey: string | symbol) {
        let val = target[propertyKey];
        const getter = () => {
            if (!val) {
                return getCustomRepository(repoClass, getConnectionName);
            }
        };
        const setter = (next) => {
            return next;
        };

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true,
        });
    }
}
export function InjectRepo<Entity>(entityClass: ObjectType<Entity>, getConnectionName?: string): PropertyDecorator {
    return function (target: any, propertyKey: string | symbol) {
        let val = target[propertyKey];
        const getter = () => {
            if (!val) {
                return getRepository(entityClass, getConnectionName || 'default');
            }
        };
        const setter = (next) => {
            return next;
        };

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true,
        });
    }
}
export function Emoji() {
    return function (target: Object, key: string | symbol) {

        let val = target[key];

        const getter = () => {
            return val;
        };
        const setter = (next) => {
            console.log('updating flavor...');
            val = `🍦 ${next} 🍦`;
        };

        Object.defineProperty(target, key, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true,
        });

    };
}

export function NewClass(): ClassDecorator {
    return function (target: Function) {
        const original = target;
        // a utility function to generate instances of a class
        function construct(constructor: Function, args: any[]) {
            const c: any = function () {
                return constructor.apply(this, args);
            };
            c.prototype = constructor.prototype;
            return new c();
        }

        // the new constructor behaviour
        const newConstructor: any = function (...args: any[]) {
            console.log("New: " + original.name);
            return construct(original, args);
        };

        // copy prototype so intanceof operator still works
        newConstructor.prototype = original.prototype;

        // return new constructor (will override original)
        return newConstructor;
    }
}