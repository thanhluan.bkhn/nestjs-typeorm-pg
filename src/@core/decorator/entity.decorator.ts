import { Column, ColumnOptions, CreateDateColumn, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ColumnCommonOptions } from "typeorm/decorator/options/ColumnCommonOptions";
import { ColumnEnumOptions } from "typeorm/decorator/options/ColumnEnumOptions";
import { ColumnHstoreOptions } from "typeorm/decorator/options/ColumnHstoreOptions";
import { ColumnNumericOptions } from "typeorm/decorator/options/ColumnNumericOptions";
import { ColumnWithLengthOptions } from "typeorm/decorator/options/ColumnWithLengthOptions";
import { ColumnWithWidthOptions } from "typeorm/decorator/options/ColumnWithWidthOptions";
import { PrimaryGeneratedColumnNumericOptions } from "typeorm/decorator/options/PrimaryGeneratedColumnNumericOptions";
import { SpatialColumnOptions } from "typeorm/decorator/options/SpatialColumnOptions";
import { ColumnType, SimpleColumnType, SpatialColumnType, WithLengthColumnType, WithPrecisionColumnType, WithWidthColumnType } from "typeorm/driver/types/ColumnTypes";
import { Core } from "../utils/convert";

function difineColumnFirst(target: Object, propertyKey: string, funDecorator: Function, options?: ColumnOptions | ColumnCommonOptions & ColumnEnumOptions) {

    // Expose()(target, propertyKey);
    if (options && options.name) {
        return funDecorator(options)(target, propertyKey)
    }

    return funDecorator({ ...options, name: Core.Utils.camelCaseToSnakeCase(propertyKey) })(target, propertyKey)
}
function difineColumnSecond(target: Object, propertyKey: string, funDecorator: Function, type: string, options?: ColumnOptions | ColumnCommonOptions & ColumnEnumOptions) {

    // Expose()(target, propertyKey);
    if (options && options.name) {
        return funDecorator(type, options)(target, propertyKey)
    }
    return funDecorator(type, { ...options, name: Core.Utils.camelCaseToSnakeCase(propertyKey) })(target, propertyKey)
}

// Column

export function DefColumn(): Function;
export function DefColumn(options?: ColumnOptions): Function;
export function DefColumn(type: SimpleColumnType, options?: ColumnCommonOptions): Function;
export function DefColumn(type: SpatialColumnType, options?: ColumnCommonOptions & SpatialColumnOptions): Function;
export function DefColumn(type: WithLengthColumnType, options?: ColumnCommonOptions & ColumnWithLengthOptions): Function;
export function DefColumn(type: WithWidthColumnType, options?: ColumnCommonOptions & ColumnWithWidthOptions): Function;
export function DefColumn(type: WithPrecisionColumnType, options?: ColumnCommonOptions & ColumnNumericOptions): Function;
export function DefColumn(type: "enum", options?: ColumnCommonOptions & ColumnEnumOptions): Function;
export function DefColumn(type: "simple-enum", options?: ColumnCommonOptions & ColumnEnumOptions): Function;
export function DefColumn(type: "set", options?: ColumnCommonOptions & ColumnEnumOptions): Function;
export function DefColumn(type: "hstore", options?: ColumnCommonOptions & ColumnHstoreOptions): Function;

export function DefColumn(type?: any, options?: ColumnOptions | ColumnCommonOptions & ColumnEnumOptions) {
    if (typeof type === "string") {
        return function (target: Object, propertyKey: string) {
            return difineColumnSecond(target, propertyKey, Column, type, options);
        }
    }
    return function (target: Object, propertyKey: string) {
        return difineColumnFirst(target, propertyKey, Column, options);
    }
}

// PrimaryColumn

export function DefPrimaryColumn(options?: ColumnOptions): Function;
export function DefPrimaryColumn(type?: ColumnType, options?: ColumnOptions): Function;
export function DefPrimaryColumn(type?: any, options?: ColumnOptions) {
    if (typeof type === "string") {
        return function (target: Object, propertyKey: string) {
            return difineColumnSecond(target, propertyKey, PrimaryColumn, type, options);
        }
    }
    return function (target: Object, propertyKey: string) {
        return difineColumnFirst(target, propertyKey, PrimaryColumn, options);
    }
}

// CreateDateColumn
export function DefCreateDateColumn(options?: ColumnOptions) {
    return function (target: Object, propertyKey: string) {
        return difineColumnFirst(target, propertyKey, CreateDateColumn, options);
    }
}

// UpdateDateColumn
export function DefUpdateDateColumn(options?: ColumnOptions) {
    return function (target: Object, propertyKey: string) {
        return difineColumnFirst(target, propertyKey, UpdateDateColumn, options);
    }
}

// PrimaryGeneratedColumn

type TStrategy = "increment" | "uuid" | "rowid"
export function DefPrimaryGeneratedColumn(): Function;
export function DefPrimaryGeneratedColumn(options?: PrimaryGeneratedColumnNumericOptions): Function;
export function DefPrimaryGeneratedColumn(strategy: TStrategy, options?: PrimaryGeneratedColumnNumericOptions): Function;
export function DefPrimaryGeneratedColumn(type?: any, options?: PrimaryGeneratedColumnNumericOptions) {
    if (typeof type === "string") {
        return function (target: Object, propertyKey: string) {
            return difineColumnSecond(target, propertyKey, PrimaryGeneratedColumn, type, options);
        }
    }
    return function (target: Object, propertyKey: string) {
        return difineColumnFirst(target, propertyKey, PrimaryGeneratedColumn, options);
    }
}
