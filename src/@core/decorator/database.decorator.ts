import { getManager } from 'typeorm';

export function BindEntityManager(dbName: string = "default") :PropertyDecorator {
    return function (target: any, propertyKey: string | symbol) {
        let val = target[propertyKey];
        const getter = () => {
            if (!val) {
                return getManager(dbName);
            }
        };
        const setter = (next) => {
            return next;
        };

        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true,
        });
    }
}