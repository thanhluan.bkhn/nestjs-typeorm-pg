import { Get, HttpCode, HttpStatus, Post, Delete, Put, Patch, Module } from "@nestjs/common";
import { METHOD_METADATA, MODULE_PATH, PATH_METADATA, } from '@nestjs/common/constants';
import { isUndefined } from "util";
import { Controller, ModuleMetadata } from "@nestjs/common/interfaces";
import {validateModuleKeys} from '@nestjs/common/utils/validate-module-keys.util'

function mergeMethod<T>(methodFunction: Function,
                        objectBy: { target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T> },
                        path?: string | string[],
                        statusCode: HttpStatus = HttpStatus.OK
) {
    const { target, propertyKey, descriptor } = objectBy
    return {
        ...HttpCode(statusCode)(target, propertyKey, descriptor),
        ...methodFunction(path)(target, propertyKey, descriptor),
    }
}

export function DefGet(path?: string | string[], statusCode: HttpStatus = HttpStatus.OK): MethodDecorator {
    return function <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) {
        return mergeMethod(Get, {
            target, propertyKey, descriptor
        },
            path,
            statusCode
        )
    }
}

export function DefPost(path?: string | string[], statusCode: HttpStatus = HttpStatus.OK): MethodDecorator {
    return function <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) {
        return mergeMethod(Post, {
            target, propertyKey, descriptor
        },
            path,
            statusCode
        )
    }
}
export function DefDelete(path?: string | string[], statusCode: HttpStatus = HttpStatus.OK): MethodDecorator {
    return function <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) {
        return mergeMethod(Delete, {
            target, propertyKey, descriptor
        },
            path,
            statusCode
        )
    }
}
export function DefPut(path?: string | string[], statusCode: HttpStatus = HttpStatus.OK): MethodDecorator {
    return function <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) {
        return mergeMethod(Put, {
            target, propertyKey, descriptor
        },
            path,
            statusCode
        )
    }
}
export function DefPatch(path?: string | string[], statusCode: HttpStatus = HttpStatus.OK): MethodDecorator {
    return function <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) {
        return mergeMethod(Patch, {
            target, propertyKey, descriptor
        },
            path,
            statusCode
        )
    }
}
