import 'reflect-metadata';
require('module-alias/register');
// import 'zone.js';
// import 'zone.js/dist/long-stack-trace-zone.js';
// import 'zone.js/dist/zone-node.js';
// import 'core-js/es/promise'
// if (!window.Promise) {
//     require('core-js/modules/es.promise');
// }
import "es6-shim";
import '~/@core/define-libs/def-query-builder';
import { AppModule } from "./app.module";
import bootServer from './boot.server';
async function run() {
    try {
        await bootServer.runApp(AppModule);
    } catch (error) {
        console.log('-------------------');
        console.log(error);
        console.log('-------------------');
    }
}
run();

