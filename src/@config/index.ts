import { DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConnectionOptions } from 'typeorm';
import * as envs from './envs';

export interface IEnvConfig {
  NAME?: string,
  BASE_MEDIA_URL?: string,
  APP: {
    port: number
  },
  REDIS?: {
    host: string, port: number, password?: string, prefix?: string, db?: number
  }
  DBS: ConnectionOptions[]
}

let envConfig: IEnvConfig = undefined;
export function configEnv(): IEnvConfig {
  if (envConfig) {
    return envConfig;
  }
  const envName = process.env.NODE_ENV || 'development';
  const currentConfig = (envs)[envName];
  return {
    ...currentConfig,
    NAME: envName
  }
};

