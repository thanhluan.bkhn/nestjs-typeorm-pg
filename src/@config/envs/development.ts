import { IEnvConfig } from '..';

const config: IEnvConfig = {
  APP: {
    port: 8002
  },
  BASE_MEDIA_URL: "",
  REDIS: {
    host: '127.0.0.1',
    port: 6379,
    password: '123456',

  },
  DBS: [
    {
      name: 'default',
      type: 'postgres',
      database: 'db_example',
      username: 'postgres',
      password: '12345',
      host: 'localhost',
      port: 5432,
      entities: [
        'build/entities/' + 'primary' + '/**/*.js',
      ],
      synchronize: true,
      logging: ["log", "error", "query", "info"],
    }
  ]
}

export default config;
