import { IEnvConfig } from '..';

const config: IEnvConfig = {
  APP: {
    port: 8002
  },
  BASE_MEDIA_URL: "",
  REDIS: {
    host: '127.0.0.1',
    port: 6379,
    password: '123456',

  },
  DBS: [
    {
      name: 'default',
      type: 'postgres',
      database: 'db_example',
      username: 'postgres',
      password: 'SML!@#123456',
      host: '192.168.1.10',
      port: 5432,
      entities: [
        'build/entities/' + 'primary' + '/**/*.js',
      ],
      synchronize: false,
      logging: ["error", "log", "info"],
    }
  ]
}

export default config;
