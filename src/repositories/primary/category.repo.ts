import { EntityRepository } from 'typeorm';
import { BaseRepository } from '~/repositories/base.repository';
import { Category } from "~/entities/primary";
@EntityRepository(Category)
export class CategoryRepo extends BaseRepository<Category> {

}