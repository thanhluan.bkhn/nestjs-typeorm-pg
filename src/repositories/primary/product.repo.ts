import { EntityRepository } from 'typeorm';
import { BaseRepository } from '~/repositories/base.repository';
import { Product } from "~/entities/primary";
@EntityRepository(Product)
export class ProductRepo extends BaseRepository<Product> {

}