import { AbstractRepository, FindConditions, FindManyOptions, JoinOptions, ObjectLiteral, Repository, Entity } from 'typeorm';

export class BaseRepository<Entity> extends Repository<Entity> {
  constructor() {
    super();
  }
  static getConnectionName() {
    return "default";
  }
}



export class BaseFixRepository<Entity extends ObjectLiteral> extends AbstractRepository<Entity> {
  constructor() {
    super();
  }

}