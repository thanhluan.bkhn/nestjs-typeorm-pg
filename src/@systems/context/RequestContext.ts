import * as cls from 'cls-hooked';
import { v4 } from "uuid";
import { Request, Response } from 'express';

export class RequestContext {
  public static nsid = v4();
  public readonly id: number;
  public request: Request;
  public response: Response;

  constructor(request: Request, response: Response) {
    this.id = Math.random();
    this.request = request;
    this.response = response;
  }

  public static currentRequestContext(): RequestContext {
    const session = cls.getNamespace(RequestContext.nsid);
    if (session && session.active) {
      return session.get(RequestContext.name);
    }
    return null;
  }
  public static currentRequest(): Request {
    return RequestContext.currentRequestContext().request
  }

  public static getHeader(key: string) {
    const req = RequestContext.currentRequest();
    const { headers = {} } = req;
    return headers[key];
  }
}
