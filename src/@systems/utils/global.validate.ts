import { ArgumentMetadata, HttpException, ValidationPipe } from "@nestjs/common";
import { Validator } from "class-validator";

interface IErrorResponse {
    message?: string[];
}
export class ValidateError extends HttpException {
    public messages: {
        [key: string]: string
    }
    constructor(messages: { [key: string]: string }, status: number) {
        super(JSON.stringify(messages), status);
        this.messages = messages
    }

}

import { validate, IsMilitaryTime, IsDefined, ValidateNested } from 'class-validator';

class Day {
    @IsMilitaryTime()
    from: string;

    @IsMilitaryTime()
    to: string;

    constructor(from: string, to: string) {
        this.from = from;
        this.to = to;
    }
}

export class DailyCalendar {
    @IsDefined()
    @ValidateNested()
    monday: Day;

    constructor(day: any) {
        this.monday = day;
    }
}

export class GlobalValidate extends ValidationPipe {
    public async transform(value, metadata: ArgumentMetadata) {
        try {
            // const day = new Day('05:30', '05:');
            // const fails = new DailyCalendar(day);
            // const passes = new DailyCalendar({ from: '05:30', to: '05' });

            // const validationErrorsFails = await validate(fails);
            // const validationErrorsPasses = await validate(passes);

            // console.log(validationErrorsFails);
            // console.log(validationErrorsPasses);

            return await super.transform(value, metadata)
        } catch (err) {
            // if (err instanceof BadRequestException) {
            //     const response = err.getResponse() as IErrorResponse;
            //     if(response && response.message) {
            //         // if(response.message.length > 0) {
            //         //     throw new BadRequestException(response.message[0])
            //         // }
            //         const messages = {};
            //         response.message.forEach(item => {
            //             const arr = item.split(/ /g);
            //             const key = arr[0];
            //             const me = item.substring(key.length);
            //             Object.assign(messages,{
            //                 [key]: me
            //             })
            //         })
            //         throw new ValidateError(messages,err.getStatus());
            //     }
            // } 
            throw err;
        }
    }
}