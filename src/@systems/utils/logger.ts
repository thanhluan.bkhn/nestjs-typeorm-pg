import { createLogger, format, transports, config } from "winston"
const { Console, File } = transports
const { colors, levels } = config.npm

export const LOGGER = createLogger({

  level: 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.errors({ stack: true }),
    format.splat(),
    format.json(),
    format.colorize({ all: true }),
    format.simple()
  ),
  defaultMeta: { service: 'nestjs-typeorm-pg' },
  transports: [
    new Console(),
    new File({ filename: './logs/error.log', level: "error" }),
    new File({ filename: './logs/combined.log' })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  LOGGER.add(new transports.Console({
    format: format.combine(
      format.colorize(),
      format.simple()
    )
  }));
}