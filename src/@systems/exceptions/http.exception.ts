import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import { ValidateError } from '../utils';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {

    catch(exception: HttpException | ValidateError | any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        let status = 500;
        if (exception instanceof HttpException) {
            status = exception.getStatus();
            if (exception instanceof ValidateError) {
                response.status(status)
                    .send({
                        code: status,
                        error: true,
                        message: exception.messages
                    })
                return;
            }
            console.log("Errorhandle", request.path, "status", status, "message:", exception.message);
            let res = exception.getResponse();
            try {
                if (response instanceof Object) {
                    if (res['statusCode']) {
                        res = Object.assign({ code: res['statusCode'] }, res);
                        if (res['statusCode'] > 300) { res["error"] = true; }
                        delete res['statusCode'];
                    }
                }
            } catch (ex) {
                console.log(ex);
            }
            response
                .status(status)
                .send(res)
            return;
        }

        if (typeof exception === 'string') {
            response
                .status(400)
                .send({
                    code: 400,
                    error: true,
                    message: exception
                });
            return;
        }


        // let status = exception.getStatus();
        response
            .status(status)
            .send({
                code: status,
                error: true,
                message: JSON.stringify(exception)
            });
        return;
    }
}