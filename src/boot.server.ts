import { INestApplication } from '@nestjs/common';
import { NestFactory, HttpAdapterHost } from '@nestjs/core';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as morgan from 'morgan';
import { configEnv, IEnvConfig } from './@config';
import { HttpExceptionFilter } from './@systems/exceptions';
import { GlobalPrefix } from './common/constants';
import { GlobalValidate } from './@systems';
import redisService from './services/@common/redis.service';
import { DocumentBuilder } from '@nestjs/swagger/dist/document-builder';
import { SwaggerModule } from '@nestjs/swagger/dist/swagger-module';

const envConfig = configEnv();
export class BootServer {
    public envConfig: IEnvConfig;
    constructor(envConfig: IEnvConfig) {
        this.envConfig = envConfig;
    }
    async runApp(appModule: any) {
        const app = await NestFactory.create(appModule);
        await this.setup(app);

    }

    // handleError = (err: Error | string, req: express.Request, res: express.Response, next: express.NextFunction) => {

    //     console.log('-------------------');
    //     console.log('-------------------');
    //     console.log("LUÂNNANNANNA");
    //     console.log('-------------------');
    //     console.log({ err });
    //     console.log('-------------------');
    //     // res.end();
    // }

    async setup(app: INestApplication) {
        const { APP, DBS, NAME, REDIS } = this.envConfig
        const { port } = APP;
        app.use(express.static('publics'));
        app.use(cookieParser());
        app.use(bodyParser.json({ limit: '50mb' }));
        app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
        app.use(compression());
        app.use(express.static('public'));

        app.use(morgan('dev'));
        app.use(cors());
        app.setGlobalPrefix(GlobalPrefix.API);

        app.useGlobalPipes(new GlobalValidate({ transform: true, whitelist: false }));
        // app.use(this.handleError)

        // const { httpAdapter } = app.get(HttpAdapterHost);

        const options = new DocumentBuilder()
            .setTitle('GOxMALL SERVICE')
            .setDescription('The GOxMALL API description')
            .setVersion('1.0')
            .addTag('GOxMALL')
            .addBearerAuth()
            .build();
        const document = SwaggerModule.createDocument(app, options);
        SwaggerModule.setup('swagger', app, document);

        app.useGlobalFilters(new HttpExceptionFilter());
        const server = await app.listen(port || 8002);
        server.setTimeout(1800000);
        try {
            redisService.init();
        } catch (error) {
        }
        console.log(`Server start on port ${port}. Open http://localhost:${port} to see results`);
        console.log(`API DOCUMENT Open http://localhost:${port}/swagger`);
    }

}
export default new BootServer(envConfig);
