import * as redis from 'redis';
import { RedisClient } from 'redis';
export interface IRedisConfig {
  host: string, port: number, password?: string, prefix?: string, db?: number
}
export class RedisHelper {
  public client: RedisClient;
  private config: IRedisConfig;
  constructor(config: IRedisConfig) {
    this.config = config;
  }
  init() {
    const { host, port, password = '', prefix, db = 0 } = this.config
    this.client = redis.createClient({
      host: host,
      port: port,
      prefix,
      db,
    });
    this.client.on("connect", (err, rs) => {
      console.log("redis Connected");
    })
    this.client.on("error", (err) => {
      console.log("redis connect error", err);
    })
  }
  hgetall = (key: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.hgetall(key, (err: Error | null, data: any) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }
  /**
   * hmset Hash Set
   * @param timeout expire time on second, default 3600
   */
  hmset = (key: string, object: any, timeout = 3600): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.hmset(key, object, (err: Error | null, reply: any) => {
        if (err) {
          reject(err);
        } else {
          this.client.expire(key, timeout);
          resolve(reply);
        }
      });
    });
  }

  /**
   * Set
   * @param timeout expire time on second, default 3600
   */
  set = (key: string, object: any, timeout = 3600): Promise<any> => {

    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.set(key, object, (err: Error | null, reply: any) => {
        if (err) {
          console.log("redis set error", key, err);
          reject(err);
        } else {
          this.client.expire(key, timeout);
          resolve(reply);
        }
      });
    });

  }
  ttl = (key: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      this.client.TTL(key, (err, data) => {
        resolve(data);
      });
    });
  }
  exist = (key: string): Promise<any> => {

    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.EXISTS(key, (err, data) => {
        if (err) {
          resolve(false);
        } else {
          resolve(data);
        }

      });
    });
  }
  get = (key: string): Promise<any> => {

    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      let d = this.client.get(key, (err, data) => {
        resolve(data);
      });
    });
  }

  getAndClearKey = (key: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.get(key, data => {
        this.client.del(key);
        resolve(data);
      });
    });
  }
  del = (key: string): Promise<any> => {
    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.del(key, (err, data) => {
        resolve(data);
      });
    });
  }
  flushall = () => {
    return new Promise((resolve, reject) => {
      if (!this.client.connected) {
        reject(new Error("Redis not connected"))
      }
      this.client.flushall((err, succeeded) => {
        if (err) {
          reject(err);
        } else {
          resolve(succeeded);
        }
      });
    });
  }
}
