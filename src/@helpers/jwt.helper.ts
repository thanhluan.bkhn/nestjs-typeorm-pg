import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
const saltRounds = 10;
export class JWTHelper {
    private privateKey: jwt.Secret;
    private publicKey: jwt.Secret;
    constructor(privateKey: jwt.Secret, publicKey: jwt.Secret) {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }
    verify = (token: string): Promise<any> => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.privateKey, {
                algorithms: ['HS256']
            }, (err, decoded) => {
                if (err) {
                    reject(new Error('Token not verify'));
                } else {
                    resolve(decoded);
                }
            });
        });
    }
    sign = (payload: string | Buffer | object): Promise<string> => {
        return new Promise((resolve, reject) => {
            jwt.sign({ payload }, this.privateKey, {
                algorithm: 'HS256',
                expiresIn: '10h'
            }, (err, decoded) => {
                if (err) {
                    reject(new Error('NOT SIGN TOKEN'));
                } else {
                    resolve(decoded);
                }
            });
        });

    }
    hash = (payload: string): Promise<string> => {
        return bcrypt.hash(payload, saltRounds);
    }
    compare = (payload: string, hash: string): Promise<boolean> => {
        return bcrypt.compare(payload, hash);
    }
}
