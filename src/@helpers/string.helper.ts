const AccentsMap = [
    "aàảãáạăằẳẵắặâầẩẫấậ",
    "AÀẢÃÁẠĂẰẲẴẮẶÂẦẨẪẤẬ",
    "dđ",
    "DĐ",
    "eèẻẽéẹêềểễếệ",
    "EÈẺẼÉẸÊỀỂỄẾỆ",
    "iìỉĩíị",
    "IÌỈĨÍỊ",
    "oòỏõóọôồổỗốộơờởỡớợ",
    "OÒỎÕÓỌÔỒỔỖỐỘƠỜỞỠỚỢ",
    "uùủũúụưừửữứự",
    "UÙỦŨÚỤƯỪỬỮỨỰ",
    "yỳỷỹýỵ",
    "YỲỶỸÝỴ"
];
export class StringHelper {
    static removeAccents = (str) => {
        for (let i = 0; i < AccentsMap.length; i++) {
            let re = new RegExp("[" + AccentsMap[i].substr(1) + "]", "g");
            let char = AccentsMap[i][0];
            str = str.replace(re, char);
        }
        return str;
    };
    static createInternalString = (text: string): string => {
        return StringHelper.removeAccents(text.trim().toLowerCase()).split(" ").filter(item => !!item).join("_");
    }
    static camelCaseToSnakeCase(text: string): string {
        return StringHelper.removeAccents(text.trim()).split(/(?=[A-Z])/).join('_').toLowerCase()
    }
    static snakeCaseToCamelCase(text: string): string {
        return StringHelper.removeAccents(text.trim()).replace(/-([a-z])/g, (ch) => { return ch[1].toUpperCase(); });
    }
    static upperCaseFirstLetter = (str: string): string => {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
}