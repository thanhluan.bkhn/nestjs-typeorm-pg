import { DynamicModule, MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import bootServer from './boot.server';
import * as allModules from './x-modules';
import { dbModules } from './@core/common';

const envConfig = bootServer.envConfig
const multipleDatabaseModule: DynamicModule[] = dbModules(envConfig.DBS);
const modules = Object.values(allModules);

const actionModules = [
    ScheduleModule.forRoot()
]
@Module({
    imports: [
        ...multipleDatabaseModule,
        ...actionModules,
        ...modules
    ]
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply()
            .forRoutes({ path: "*", method: RequestMethod.ALL })
    }
}
