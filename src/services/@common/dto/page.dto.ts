import { ValidateNested, isDefined, IsDefined } from "class-validator";
import { Type } from "class-transformer";
type TOperator = '=' | '!=' | '<' | '>' | '>=' | '<=' | 'like' | 'in' | 'notIn' | 'between'
type TValueBasic = number | string | Date | boolean
type TBetwen = {
    from: TValueBasic;
    to: TValueBasic
}
type TValue = TValueBasic | TBetwen

class FilterOption {
    value: TValue;
    operator?: TOperator = '='
}
export type IFilter<TObject = any> = {
    [key in keyof TObject]?: FilterOption
}

export class PageRequest<TObject> {
    pageIndex?: number = 1;
    pageSize?: number = 10;
    @ValidateNested()
    @IsDefined()
    filters: TObject
}
export class PageResponse<TData> {
    data: TData[];
    total: number;
}