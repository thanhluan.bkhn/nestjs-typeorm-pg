
import { BindEntityManager, Service } from "~/@core/decorator";
import { EntityManager, In } from "typeorm";
import { PageResponse } from "./dto/page.dto";
import { configEnv } from "~/@config";
const envConfig = configEnv();
@Service()
export class SharedService {
  @BindEntityManager()
  private em: EntityManager
  async paginationNativeQuery<TypeData = any>(options: {
    sql: string,
    pageIndex?: number,
    pageSize?: number
  }) {
    const { sql, pageIndex = 1, pageSize = 10 } = options
    const offset = (pageIndex - 1) * pageSize;
    const sqlData = ` select * from ( ${sql} ) tbTemp limit ${pageSize} offset ${offset} `;
    const sqlTotal = ` select count(*) as total from ( ${sql} ) tbTemp `;
    const result = await Promise.all([
      this.em.query(sqlData),
      this.em.query(sqlTotal)
    ])
    const output = new PageResponse<TypeData>();
    output.data = result[0];
    output.total = result[1][0].total
    return output

  }
}

export default new SharedService();
