import { RedisHelper } from "~/@helpers/redis.helper";
import { configEnv } from "~/@config";
const { REDIS } = configEnv()
export default new RedisHelper(REDIS);


