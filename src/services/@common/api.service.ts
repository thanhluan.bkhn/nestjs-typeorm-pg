
import { configEnv } from "~/@config";
import { AxiosHttpClient, IAxiosRequestOptions } from "~/@core/network/AxiosHttpClient";
import { IObjectPromise } from "~/@core/utils/shared";

class ApiService {
  private httpClient: AxiosHttpClient

  constructor(config: {
    baseurl: string, options: IAxiosRequestOptions, interceptors?: IObjectPromise
  }) {
    this.httpClient = new AxiosHttpClient(config)
  }
  async get<T = any>(endpoint: string, params: any = {}): Promise<T> {
    try {
      const res = await this.httpClient.get<T>(endpoint, params);
      return res.data
    } catch (error) {
      throw error;
    }
  }
  async getByBody<T = any>(endpoint: string, body: any = {}): Promise<T> {
    try {
      const res = await this.httpClient.getByBody<T>(endpoint, body);
      return res.data
    } catch (error) {
      throw error;
    }
  }
  async post<T = any>(endpoint: string, body: any = {}): Promise<T> {
    try {
      const res = await this.httpClient.post<T>(endpoint, body);
      return res.data;
    } catch (error) {
      throw error;
    }
  }

  async put<T = any>(endpoint: string, body: any = {}): Promise<T> {
    try {
      const res = await this.httpClient.put<T>(endpoint, body);
      return res.data
    } catch (error) {
      throw error;
    }
  }

}
