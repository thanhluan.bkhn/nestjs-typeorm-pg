import { Service, InjectCustomRepo, BindEntityManager, InjectService } from "~/@core/decorator";
import { EntityManager, Equal, In, Not, Transaction, TransactionManager } from "typeorm";
import { ProductRepo } from "~/repositories/primary";
import { ProductReq } from "./dto/product.req";



@Service()
export class ProductService {

    @BindEntityManager()
    private em: EntityManager

    @InjectCustomRepo(ProductRepo)
    private repo: ProductRepo



    list(body: ProductReq) {
        return this.repo.find({
            where: {
                ...body
            }
        });
    }

}