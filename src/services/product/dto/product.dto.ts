import { IsNotEmpty } from "class-validator";

export class ProductDto {
    @IsNotEmpty()
    whseid: string;

    @IsNotEmpty()
    storerkey: string;

    @IsNotEmpty()
    refId: string;

    @IsNotEmpty()
    taskType: string;
}