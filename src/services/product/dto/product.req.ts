import { ApiProperty } from "@nestjs/swagger";

export class ProductReq {
    @ApiProperty()
    id: number;
    @ApiProperty()
    name: string
}