import { Body, Controller } from "@nestjs/common";
import { DefGet } from "~/@core/decorator";


@Controller("category")
export class CategoryController {

    @DefGet("list")
    async list(@Body() body: { name: string, id: number }) {
        return [];
    }
}