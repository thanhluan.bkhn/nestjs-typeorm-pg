import { ChildModule } from '~/@core/decorator';
import { CategoryController } from './category.controller';
import { ProductController } from './product.controller';


@ChildModule({
    prefix: "public",
    controllers: [
        ProductController,
        CategoryController,
    ],
})
export class PublicModule {
}
