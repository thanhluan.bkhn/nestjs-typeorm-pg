import { Body, Controller } from "@nestjs/common";
import { ApiBody, ApiResponse } from "@nestjs/swagger";
import { DefGet, DefPost, InjectService } from "~/@core/decorator";
import { ProductService } from "~/services";
import { ProductReq } from "~/services/product/dto/product.req";


@Controller("product")
export class ProductController {


    @InjectService('ProductService')
    private productService: ProductService

    @DefPost("list")
    async list(@Body() body: ProductReq) {
        return this.productService.list(body)
    }
}