import { Column, Entity } from "typeorm";
import { BaseEntityPostgres, BaseEntitySql } from "../base.entity";

@Entity("category")
export class Product extends BaseEntitySql {

    @Column()
    categoryId: number

    @Column()
    name: string
}
