import { Column, Entity } from "typeorm";
import { BaseEntityPostgres } from "../base.entity";

@Entity("category")
export class Category extends BaseEntityPostgres {
    @Column()
    name: string
}
