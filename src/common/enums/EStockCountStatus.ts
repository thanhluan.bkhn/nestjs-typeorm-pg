export const EStockCountStatus = {
    NEW: 0,
    FIRST: 2,
    SECOND: 3,
    THIRD: 4,
    DONE: 9,
    CLOSE: -1
}