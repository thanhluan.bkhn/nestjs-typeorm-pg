export const EValidateTypeCode = {
    CreateAsn: 'CreateAsn',
    ReceiveAsn: 'ReceiveAsn',
}

export const  EValidateStatus = {
    None: 0,
    Required: 1,
    Exclusively: 2,
    RequiredAndExclusively: 3
}