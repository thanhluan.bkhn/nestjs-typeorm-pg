export const ESaleorderStatus = {
    New: '04',
    PartAllocate: 14,
    Allocated: 17,
    PartPick: 52,
    Picked: 55,
    PartShipped: 92,
    ShipCompleted: 95,
    Cancel: '11'
}