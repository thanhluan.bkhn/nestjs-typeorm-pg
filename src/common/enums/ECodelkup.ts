export const ECodelkup = {
    STATUSRECEIPTDETAIL: 'STATUSRECEIPTDETAIL',
    ORDERTYPE: 'ORDERTYPE',
    RECEIPTYPE: 'RECEIPTYPE',
    REASONCODE: 'REASONCODE',
    PROCESSCODE: 'PROCESSCODE',
    PROCESSORDERCODE: 'PROCESSORDERCODE',
    REASONORDERCODE: 'REASONORDERCODE',
}