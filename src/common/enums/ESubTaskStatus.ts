export const ESubTaskStatus = {
    New: 1,
    InProgress: 2,
    Done: 3,
    Reject: 4
}