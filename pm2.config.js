module.exports = {
    apps: [
        {
            name: 'backend',
            script: './build/main.js',
            instances: "1", //"max", // "2" 
            autorestart: true,
            watch: false,
            max_memory_restart: '2G',
            out_file: "./../logs/backend/access.log",
            error_file: "./../logs/backend/error.log",
            log_date_format: "YYYY-MM-DD HH:mm:ss",
            merge_logs: true,
            env: {
                NODE_ENV: 'development'
            },
            env_qa: {
                NODE_ENV: 'qa'
            },
            env_production: {
                NODE_ENV: 'production'
            },
        },
    ],
};